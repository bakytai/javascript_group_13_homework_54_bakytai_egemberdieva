import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cell',
  templateUrl: './cell.component.html',
  styleUrls: ['./cell.component.css']
})
export class CellComponent {
  @Input() string = '';
  class = 'black';

  onClick() {
    this.class = 'white';
  }

  cellClass() {
    return 'cell ' + this.class;
  }
}
