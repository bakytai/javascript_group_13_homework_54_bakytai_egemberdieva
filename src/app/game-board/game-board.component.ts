import { Component } from '@angular/core';
import {Cell} from "./cellObject.model";

@Component({
  selector: 'app-game-board',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.css']
})
export class GameBoardComponent {
  cellArray: Cell[] = [];
  tries = 0;
  random = 0;
  value = false;
  constructor() {
    this.getNewArray();
  }

  getNewArray() {
    this.cellArray.splice(0,36)
    for (let i = 0; i < 36; i++) {
      const cell = new Cell();
      this.cellArray.push(cell);
    }
    this.random = Math.floor(Math.random() * this.cellArray.length);
    this.cellArray[this.random].hasItem = true;
  }

 getSymbol(index: number){
    if (this.cellArray[index].hasItem) {
      return 'O';
    } else {
      return '';
    }
 }

 getTries(index: number) {
    if (this.cellArray[index].key === false) {
      this.tries++;
      this.cellArray[index].key = true;
    }
    if (this.cellArray[index].hasItem === true) {
      this.value = true;
    }
 }

  onReset() {
    this.value = false;
    this.tries = 0;
    this.getNewArray();
  }

  getClass() {
    if (this.value === true) {
      const className = 'disabled';
      return 'container ' + className;
    } else {
      return 'container'
    }
  }
}
